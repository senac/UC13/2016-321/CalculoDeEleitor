
package br.com.senac.calculodeeleitor;

import static org.junit.Assert.*;
import org.junit.Test;

public class CalculadoraDeEleitorTest {

    @Test
    public void naoDeveSerEleitor() {
        int idade  = 15 ;
        CalculadoraDeEleitor calculadora = new CalculadoraDeEleitor();
        String resultado  = calculadora.calcular(idade);
        assertEquals(CalculadoraDeEleitor.NAO_ELEITOR, resultado);
          
    }
    
    @Test
    public void deveSerEleitor() {
        int idade  = 25 ;
        CalculadoraDeEleitor calculadora = new CalculadoraDeEleitor();
        String resultado  = calculadora.calcular(idade);
        assertEquals(CalculadoraDeEleitor.ELEITOR_OBRIGATORIO, resultado);
          
    }
    
    @Test
    public void deveSerEleitorFacultativoJovem() {
        int idade  = 16 ;
        CalculadoraDeEleitor calculadora = new CalculadoraDeEleitor();
        String resultado  = calculadora.calcular(idade);
        assertEquals(CalculadoraDeEleitor.ELEITOR_FACULTATIVO, resultado);
          
    }
    
    
    @Test
    public void deveSerEleitorFacultativoIdoso() {
        int idade  = 66 ;
        CalculadoraDeEleitor calculadora = new CalculadoraDeEleitor();
        String resultado  = calculadora.calcular(idade);
        assertEquals(CalculadoraDeEleitor.ELEITOR_FACULTATIVO, resultado);
          
    }
    
    
}
